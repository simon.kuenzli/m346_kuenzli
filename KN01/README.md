## Aufgaben KN01

#### Dies sind meine Prozessoren:

![alt text](Prozessoren.png "Prozessoren")

#### Dies ist mein Arbeitsspericher:

![alt text](RAM.png "CPU")

#### Das passiert, wenn ich die VM mit weniger Prozessoren als mein Laptop starte und den Befehl lscpu | grep "CPU(s) ausführe.

![alt text](VM_mit_weniger_Prozessoren.png "Weniger_Prozessoren")

#### Wenn ich mehr Prozessoren für die VM als mein Laptop angebe, kann ich die VM gar nicht starten und bekomme diese Fehlermeldung.

![alt text](VM_mit_mehr_Prozessoren.png "Mehr_Prozessoren")

#### Das passiert, wenn ich die VM mit weniger Arbeitsspeicher als mein Laptop starte und den Befehl free -g ausführe.

![alt text](VM_mit_weniger_RAM.png "Weniger_CPU")

#### Auch wenn ich mehr Arbeitsspeicher als mein Laptop angebe, kann ich die VM nicht starten und bekomme folgende Fehlermeldung.

![alt text](VM_mit_mehr_RAM.png "Mehr_CPU")

#### Meine Schlussfolgerung: 
Der Computer kann keine Prozessoren simulieren, die er selbst gar nicht hat. Das Gleiche gilt für den Arbeitsspeicher, da der Laptop dann keinen Arbeitsspeicher für seine eigenen Prozesse hat.