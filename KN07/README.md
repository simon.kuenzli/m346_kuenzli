##Dies sind die Preise für den Web-Server und die Datenbank bei AWS:
Web-Server:
![alt text](WebServer_AWS.png "WebServer_AWS")
Datenbank:
![alt text](DB_AWS.png "DB_AWS")



##Folgend die Preise von Azure:
Web-Server:
![alt text](WebServer_Azure.png "WebServer_Azure")
Datenbank:
![alt text](DB_Azure.png "DB_Azure")

Die Preise für den Web-Server sind ziemlich ausgeglichen, also würden beide Anbieter funktionieren.
Bei der Datenbank auf AWS muss man zu Begin einen hohen Betrag zahlen, aber die monatlichen Kosten sind dafür tief.
Bei der Azure Datenbank gibt es nur monatliche Kosten, die aber hoch sind.
Wenn man die Datenbank für eine längere Zeit braucht, würde sich die Datenbank auf AWS mehr lohnen.


##Die Preise, um den Web-Server und die Datenbank auf Heroku zu hosten:
Web-Server:
![alt text](WebServer_Heroku.png "WebServer_Heroku")
Datenbank:
![alt text](DB_Heroku.png "DB_Heroku")